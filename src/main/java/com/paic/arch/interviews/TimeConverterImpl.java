package com.paic.arch.interviews;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;

/**
 * 时间转路灯实现类
 * @author Jia
 * @date 2018/4/11
 */
public class TimeConverterImpl implements TimeConverter {
    protected Logger log= LoggerFactory.getLogger(TimeConverterImpl.class);

    private int hour;
    private int minter;
    private int second;
    //返回结果
    private StringBuffer result = new StringBuffer();

    /**
     * O代表灯不亮，R代表亮红灯，Y代表亮黄灯，r1代表第一排，默认全部不亮
     */
    String[] r1 = {"O"};
    /**
     * r2代表第2排4栈灯，默认全部不亮
     */
    String[] r2 = {"O", "O", "O", "O"};
    /**
     * r3代表第3排4栈灯，默认全部不亮
     */
    String[] r3 = {"O", "O", "O", "O"};
    /**
     * r4代表第4排11栈灯，默认全部不亮
     */
    String[] r4 = {"O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O"};
    /**
     * r5代表第5排4栈灯，默认全部不亮
     */
    String[] r5 = {"O", "O", "O", "O"};

    @Override
    public String convertTime(String aTime) {

        //1.将输入时间按时分秒解析
        parseTime(aTime);
        //2.时分秒分别转换为对应的亮灯数
        convertHourToLight();
        convertMinuteToLight();
        convertSecondToLight();
        //3.转换为跟测试用例对应的数据结构
        getResult(r1,r2,r3,r4,r5);
        return result.toString();
    }

    private void parseTime(String aTime){
        try {
            //24:00:00时间不存在，需特殊处理
            if ("24:00:00".equals(aTime)) {
                hour = 24;
                minter = 0;
                second = 0;
            } else {
                LocalTime time = LocalTime.parse(aTime);
                hour = time.getHour();
                minter = time.getMinute();
                second = time.getSecond();
            }
        }catch (Exception e){
            log.error("无效的时间:{}",aTime);
            throw new RuntimeException("输入的时间无效");
        }
    }

    /**
     * 把小时数转化为灯亮
     */
    private void convertHourToLight() {
        //第二排每盏灯代表五个小时，亮红灯
        for (int i = 0; i < hour / 5; i++) {
            r2[i] = "R";
        }
        //第二排除后剩下的放在第三排，每盏灯代表1个小时，亮红灯
        for (int i = 0; i < hour % 5; i++) {
            r3[i] = "R";
        }
    }

    /**
     * 把分钟数转化为灯亮
     */
    private void convertMinuteToLight() {
        //第四排每盏灯代表五分钟，每隔三栈亮一次红灯，其余亮黄灯
        for (int i = 0; i < minter / 5; i++) {
            if ((i + 1) % 3 == 0) {
                r4[i] = "R";
            } else {
                r4[i] = "Y";
            }
        }
        //第四排除剩的放在第五排，亮黄灯
        for (int i = 0; i < minter % 5; i++) {
            r5[i] = "Y";
        }
    }

    /**
     * 把秒钟转化为灯亮
     */
    private void convertSecondToLight() {
        //偶数则亮灯
        if (second % 2 == 0) {
            r1[0] = "Y";
        }
    }

    private void getResult(String[]... rows){
        for (int i = 0; i <rows.length ; i++) {
            String[] row =rows[i];
            for (int y = 0; y <row.length; y++) {
                result.append(row[y]);
            }
            if(row==r1 ||row==r2||row==r3||row==r4){
                result.append("\r\n");
            }
        }
    }

}
